'use strict';


var IndexModel = require('../models/index');


module.exports = function (app) {

    var model = new IndexModel();


    app.get('/', function (req, res) {
        res.render('index', model); 
    });

    app.get('/app/activity', function (req, res) {        
        res.render('appactivity', model);
    });

    app.get('/app/lifecycle', function (req, res) {        
        res.render('applifecycle', model);
    });

    app.get('/app/team', function (req, res) {        
        res.render('appteam', model);
    });

    app.get('/app/monitor', function (req, res) {        
        res.render('appmonitor', model);
    });

    app.get('/app/deployment', function (req, res) {        
        res.render('appdeployment', model);
    });

    app.get('/app/dependency', function (req, res) {        
        res.render('appdependency', model);
    });

    app.get('/app/billing', function (req, res) {        
        res.render('appbilling', model);
    });

    app.get('/app/setting', function (req, res) {        
        res.render('appsetting', model);
    });

};
